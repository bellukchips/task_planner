part of 'extensions.dart';

extension DateTimeExtension on DateTime {
  String get dateAndTime =>
      "${this.monthName} ${this.day}, ${this.year} - ${this.hourFormat}:${this.second} ${(this.hour > 12) ? 'PM' : 'AM'}";
  String get shortDayName {
    switch (this.weekday) {
      case 1:
        return 'Mon';
      case 2:
        return 'Tue';
      case 3:
        return 'Wed';
      case 4:
        return 'Thu';
      case 5:
        return 'Fri';
      case 6:
        return 'Sat';
      default:
        return 'Sun';
    }
  }

  int get hourFormat {
    switch (this.hour) {
      case 1:
        return 1;
        break;
      case 2:
        return 2;
        break;
      case 3:
        return 3;
        break;
      case 4:
        return 4;
        break;
      case 5:
        return 5;
        break;
      case 6:
        return 6;
        break;
      case 7:
        return 7;
        break;
      case 8:
        return 8;
        break;
      case 9:
        return 9;
        break;
      case 10:
        return 10;
        break;
      case 11:
        return 11;
        break;
      case 12:
        return 12;
        break;
      case 13:
        return 1;
        break;
      case 14:
        return 2;
        break;
      case 15:
        return 3;
        break;
      case 16:
        return 4;
        break;
      case 17:
        return 5;
        break;
      case 18:
        return 6;
        break;
      case 19:
        return 7;
        break;
      case 20:
        return 8;
        break;
      case 21:
        return 9;
        break;
      case 22:
        return 10;
        break;
      case 23:
        return 11;
        break;
      default:
        return 12;
        break;
    }
  }

  String get monthName {
    switch (this.month) {
      case 1:
        return 'Jan';
      case 2:
        return 'Fev';
      case 3:
        return 'Mar';
      case 4:
        return 'Apr';
      case 5:
        return 'May';
      case 6:
        return 'Jun';
      case 7:
        return 'Jul';
      case 8:
        return 'Aug';
      case 9:
        return 'Sep';
      case 10:
        return 'Oct';
      case 11:
        return 'Nov';
      default:
        return 'Dec';
    }
  }
}
