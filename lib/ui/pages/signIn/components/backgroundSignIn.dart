part of '../../pages.dart';

class BackgroundSignIn extends StatelessWidget {
  const BackgroundSignIn({Key key, @required this.child}) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        height: size.height,
        width: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
                left: 0,
                top: 50,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacementNamed(context, '/');
                  },
                  child: Image.asset(
                    ThemeApplication.imageAsset + "icon_back.png",
                  ),
                )),
            Positioned(
                top: 0,
                right: 0,
                child: Image.asset(
                    ThemeApplication.imageAsset + "signin_top.png",
                    width: ScreenUtil().setWidth(200))),
            Positioned(
                left: 0,
                bottom: 0,
                child: Image.asset(
                    ThemeApplication.imageAsset + "signin_bottom.png",
                    width: ScreenUtil().setWidth(200))),
            child,
          ],
        ));
  }
}
