part of '../../pages.dart';

class BodySignIn extends StatelessWidget {
  const BodySignIn({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(250),
            ),
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: ScreenUtil().setWidth(350),
                  child: Text(
                    'Hello!\nSign in to\nGet started',
                    textAlign: TextAlign.left,
                    style: ThemeApplication.greyPoppins.copyWith(
                        fontSize:
                            ScreenUtil().setSp(60, allowFontScalingSelf: true),
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: ButtonFlat(
              image: Image.asset(ThemeApplication.imageAsset + "ic_google.png"),
              height: ScreenUtil().setHeight(90.0),
              width: ScreenUtil().setWidth(470.0),
              margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(130.0),
                  bottom: ScreenUtil().setHeight(20.0)),
              color: ThemeApplication.blueGrey,
              onPressed: () {
                alertLoading(context);
                Timer(Duration(seconds: 2), () {
                  Navigator.of(context);
                  Navigator.pushReplacementNamed(context, '/dashboard');
                });
              },
              textStyle: ThemeApplication.whitePoppins.copyWith(
                  fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                  fontWeight: FontWeight.bold),
              title: "Continue with google",
            ),
          )
        ],
      ),
    );
  }
}
