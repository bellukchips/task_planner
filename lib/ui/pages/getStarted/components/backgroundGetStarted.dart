part of '../../pages.dart';

class BackgroundGetStarted extends StatelessWidget {
  final Widget child;
  const BackgroundGetStarted({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        height: size.height,
        width: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  ThemeApplication.imageAsset + "main_top.png",
                  width: ScreenUtil().setWidth(220),
                )),
            Positioned(
                right: 0,
                child: Image.asset(
                  ThemeApplication.imageAsset + "main_side.png",
                )),
            child,
          ],
        ));
  }
}
