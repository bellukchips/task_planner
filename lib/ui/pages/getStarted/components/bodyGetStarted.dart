part of '../../pages.dart';

class BodyGetStarted extends StatelessWidget {
  const BodyGetStarted({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(200),
            ),
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: ScreenUtil().setHeight(450.0),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                    ThemeApplication.imageAsset + "get_started.png",
                  ))),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(21.0)),
                  child: Text(
                    "Manage your Task",
                    style: ThemeApplication.greyPoppins
                        .copyWith(fontSize: 40.sp, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(21.0),
                      bottom: ScreenUtil().setHeight(16.0)),
                  child: Text(
                    "You can schedule tasks\nif you don't want to forget to do them",
                    style: ThemeApplication.blackPoppins.copyWith(
                      fontSize:
                          ScreenUtil().setSp(30, allowFontScalingSelf: true),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                ButtonFlat(
                  height: ScreenUtil().setHeight(90.0),
                  width: ScreenUtil().setWidth(500.0),
                  margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(73.0),
                      bottom: ScreenUtil().setHeight(20.0)),
                  color: ThemeApplication.blueGrey,
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/signIn');
                  },
                  textStyle: ThemeApplication.whitePoppins.copyWith(
                      fontSize:
                          ScreenUtil().setSp(30, allowFontScalingSelf: false),
                      fontWeight: FontWeight.bold),
                  title: "Let's Go",
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
