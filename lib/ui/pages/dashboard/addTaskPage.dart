part of './../pages.dart';

class AddTaskPage extends StatefulWidget {
  @override
  _AddTaskPageState createState() => _AddTaskPageState();
}

class _AddTaskPageState extends State<AddTaskPage> {
  ///
  ///
  ///variable
  String labelText;
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  ValueChanged<DateTime> selectDate;
  ValueChanged<TimeOfDay> selectTime;
  TextEditingController tasktitle = TextEditingController();
  String labelTaskTitle = "";

  ///
  ///
  ///
  /// function to select date
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2015),
      initialDatePickerMode: DatePickerMode.day,
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  ///
  ///function to selected time
  ///
  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      setState(() {
        selectedTime = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle valueStyle = Theme.of(context).textTheme.body1;
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/dashboard');
        return;
      },
      child: Scaffold(
        body: BackgroundAddTask(
          child: Container(
            margin: EdgeInsets.only(top: 120),
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Create new task",
                          style: ThemeApplication.greyPoppins.copyWith(
                            fontSize: ScreenUtil()
                                .setSp(60, allowFontScalingSelf: true),
                            fontWeight: FontWeight.bold,
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      Text("Title task",
                          style: ThemeApplication.greyPoppins.copyWith(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                          )),
                      TextField(
                        onChanged: (e) {
                          setState(() {
                            labelTaskTitle = e;
                          });
                        },
                        cursorColor: ThemeApplication.fontColorPrimary,
                        decoration: InputDecoration(
                          fillColor: ThemeApplication.grey,
                          filled: true,
                          hintText: "Title task",
                          labelText: "",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5),
                            borderSide: new BorderSide(
                              width: 0,
                              color: ThemeApplication.grey,
                            ),
                          ),
                          border: new OutlineInputBorder(),
                        ),
                        maxLength: 15,
                      ),
                      Text("Description task",
                          style: ThemeApplication.greyPoppins.copyWith(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                          )),
                      TextField(
                        keyboardType: TextInputType.multiline,
                        cursorColor: ThemeApplication.fontColorPrimary,
                        decoration: InputDecoration(
                          fillColor: ThemeApplication.grey,
                          filled: true,
                          hintText: "Description task",
                          labelText: "",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5),
                            borderSide: new BorderSide(
                              width: 0,
                              color: ThemeApplication.grey,
                            ),
                          ),
                          border: new OutlineInputBorder(),
                        ),
                        maxLength: 1000,
                        maxLines: 8,
                      ),
                      Text(
                        "Select Card Colors",
                        style: ThemeApplication.greyPoppins.copyWith(
                          fontSize: ScreenUtil()
                              .setSp(35, allowFontScalingSelf: true),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            height: ScreenUtil().setHeight(50),
                            width: ScreenUtil().setWidth(50),
                            decoration: BoxDecoration(
                              color: ThemeApplication.bluegreyAccent2,
                              shape: BoxShape.circle,
                              border:
                                  Border.all(color: ThemeApplication.greenDark),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            height: ScreenUtil().setHeight(50),
                            width: ScreenUtil().setWidth(50),
                            decoration: BoxDecoration(
                              color: ThemeApplication.orangeAccent2,
                              shape: BoxShape.circle,
                              // border: Border.all(color: ThemeApplication.greenDark),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            height: ScreenUtil().setHeight(50),
                            width: ScreenUtil().setWidth(50),
                            decoration: BoxDecoration(
                              color: ThemeApplication.purpleAccent,
                              shape: BoxShape.circle,
                              // border: Border.all(color: ThemeApplication.greenDark),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Choose date & time",
                        style: ThemeApplication.greyPoppins.copyWith(
                          fontSize: ScreenUtil()
                              .setSp(35, allowFontScalingSelf: true),
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Expanded(
                            flex: 4,
                            child: new _InputDropdown(
                              labelText: labelText,
                              valueText:
                                  DateFormat.yMMMd().format(selectedDate),
                              valueStyle: valueStyle,
                              onPressed: () {
                                _selectDate(context);
                              },
                            ),
                          ),
                          const SizedBox(width: 12.0),
                          new Expanded(
                            flex: 3,
                            child: new _InputDropdown(
                              valueText: selectedTime.format(context),
                              valueStyle: valueStyle,
                              onPressed: () {
                                _selectTime(context);
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Preview",
                        style: ThemeApplication.greyPoppins.copyWith(
                          fontSize: ScreenUtil()
                              .setSp(35, allowFontScalingSelf: true),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                CardTask(
                  color: ThemeApplication.bluegreyAccent2,
                  title: labelTaskTitle,
                  lastest: "Today",
                  time: DateFormat.yMMMd().format(selectedDate) +
                      " - " +
                      selectedTime.format(context),
                  fontColor: ThemeApplication.bluegreyDark,
                  fontColorTitle: ThemeApplication.greenDark,
                  fontSizeTitle: ScreenUtil().setSp(30),
                  fontSizeSub: ScreenUtil().setSp(23),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: defaultMargin,
                  ),
                  child: ButtonFlat(
                    height: ScreenUtil().setHeight(90.0),
                    width: ScreenUtil().setWidth(500.0),
                    margin: EdgeInsets.only(
                        top: ScreenUtil().setHeight(73.0),
                        bottom: ScreenUtil().setHeight(20.0)),
                    color: ThemeApplication.blueGrey,
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/dashboard');
                    },
                    textStyle: ThemeApplication.whitePoppins.copyWith(
                        fontSize:
                            ScreenUtil().setSp(30, allowFontScalingSelf: false),
                        fontWeight: FontWeight.bold),
                    title: "Save your task",
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _InputDropdown extends StatelessWidget {
  const _InputDropdown(
      {Key key,
      this.child,
      this.labelText,
      this.valueText,
      this.valueStyle,
      this.onPressed})
      : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: onPressed,
      child: new InputDecorator(
        decoration: new InputDecoration(
          labelText: labelText,
        ),
        baseStyle: valueStyle,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text(valueText, style: valueStyle),
            new Icon(Icons.arrow_drop_down,
                color: Theme.of(context).brightness == Brightness.light
                    ? ThemeApplication.fontColorPrimary
                    : Colors.white70),
          ],
        ),
      ),
    );
  }
}
