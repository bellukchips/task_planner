part of './../pages.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  int bottomNavbarIndex = 0;

  ///
  /// widget select page
  Widget callPage(int index) {
    switch (index) {
      case 0:
        return HomePage();
      case 1:
        return HistoryPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Stack(
        children: [
          callPage(bottomNavbarIndex),
          createCustomBottomnavBar(context),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 46,
              width: 46,
              margin: EdgeInsets.only(bottom: 42),
              child: FloatingActionButton(
                  elevation: 0,
                  backgroundColor: ThemeApplication.greenAccent,
                  child: SizedBox(
                    height: 26,
                    width: 26,
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    Navigator.pushReplacementNamed(context, '/addNewTask');
                  }),
            ),
          )
        ],
      ),
    ));
  }

  /// bottom nav bar

  Widget createCustomBottomnavBar(BuildContext context) => Align(
        alignment: Alignment.bottomCenter,
        child: ClipPath(
          clipper: BottomNavbarClipper(),
          child: Container(
            height: 70,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25), topRight: Radius.circular(25)),
            ),
            child: BottomNavigationBar(
              elevation: 0,
              backgroundColor: Colors.transparent,
              selectedItemColor: ThemeApplication.blueGrey,
              unselectedItemColor: Color(0xffe5e5e5),
              currentIndex: bottomNavbarIndex,
              onTap: (index) {
                setState(() {
                  bottomNavbarIndex = index;
                });
              },
              items: [
                BottomNavigationBarItem(
                    title: Text("Dashboard"),
                    icon: Container(
                      margin: EdgeInsets.only(bottom: 6),
                      height: 30,
                      child: Image.asset((bottomNavbarIndex == 0)
                          ? ThemeApplication.imageAsset + "ic_home_active.png"
                          : ThemeApplication.imageAsset + "ic_home.png"),
                    )),
                BottomNavigationBarItem(
                    title: Text(
                      "History",
                    ),
                    icon: Container(
                      margin: EdgeInsets.only(bottom: 6),
                      height: 20,
                      child: Image.asset((bottomNavbarIndex == 1)
                          ? ThemeApplication.imageAsset +
                              "ic_history_active.png"
                          : ThemeApplication.imageAsset + "ic_history.png"),
                    ))
              ],
            ),
          ),
        ),
      );
}

class BottomNavbarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width / 2 - 28, 0);
    path.quadraticBezierTo(size.width / 2 - 28, 33, size.width / 2, 33);
    path.quadraticBezierTo(size.width / 2 + 28, 33, size.width / 2 + 28, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
