part of './../../pages.dart';

class BodyHomePage extends StatelessWidget {
  const BodyHomePage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: [HeaderHome(), CardScroll()],
      ),
    );
  }
}

class CardScroll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 2 * defaultMargin - 100,
      child: CardTaskBundle(),
    );
  }
}

class CardTaskBundle extends StatelessWidget {
  const CardTaskBundle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: defaultMargin),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CardCountTask(
                height: 130,
                width: (MediaQuery.of(context).size.width -
                        2 * defaultMargin -
                        5) /
                    2,
                fontSizeTitle: ScreenUtil().setSp(33),
                fontSizeNumber: ScreenUtil().setSp(30),
                title: "Task\ncomplete",
                subTitle: "50 task",
                color: ThemeApplication.blueGrey.withOpacity(0.6),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(5),
              ),
              CardCountTask(
                height: 130,
                width: (MediaQuery.of(context).size.width -
                        2 * defaultMargin -
                        5) /
                    2,
                fontSizeTitle: ScreenUtil().setSp(33),
                fontSizeNumber: ScreenUtil().setSp(30),
                title: "Task\non progress",
                subTitle: "5 task",
                color: ThemeApplication.pinkAccent.withOpacity(0.6),
              ),
            ],
          ),
        ),
        TotalTaskCard(),
        Container(
          margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 5),
          child: Text(
            "New task",
            style: ThemeApplication.greyPoppins.copyWith(
                fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                fontWeight: FontWeight.w900),
          ),
        ),
        Expanded(
            child: Container(
          child: ListView(
              physics: BouncingScrollPhysics(),
              children: dumyTask
                  .map((e) => Padding(
                    padding:EdgeInsets.only(bottom: (e == dumyTask.last) ? 100 : 0),
                    child: (e.status == true) ? CardTask(
                          color: ThemeApplication.purpleAccent,
                          title: e.title,
                          lastest: "Today",
                          time: e.time.dateAndTime,
                          fontColor: ThemeApplication.purpleDark,
                          fontColorTitle: ThemeApplication.purple,
                          fontSizeTitle: ScreenUtil().setSp(30),
                          fontSizeSub: ScreenUtil().setSp(23),
                        ) : Container()
                  ))
                  .toList()),
        )),
      ],
    );
  }
}

class TotalTaskCard extends StatelessWidget {
  const TotalTaskCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ThemeApplication.green.withOpacity(0.6),
      ),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
                width: ScreenUtil().setWidth(150),
                height: ScreenUtil().setHeight(150),
                child:
                    Image.asset(ThemeApplication.imageAsset + "ic_total.png")),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Total task",
                    style: ThemeApplication.whitePoppins.copyWith(
                        fontSize:
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    width: 110,
                    child: Text(
                      "55 task",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: ThemeApplication.whitePoppins.copyWith(
                        fontSize:
                            ScreenUtil().setSp(30, allowFontScalingSelf: true),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class HeaderHome extends StatelessWidget {
  const HeaderHome({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: defaultMargin),
          width: ScreenUtil().setWidth(350),
          child: Text(
            dumyUser.name,
            style: ThemeApplication.greyPoppins.copyWith(
                fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                fontWeight: FontWeight.w900),
            overflow: TextOverflow.visible,
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: defaultMargin),
          height: ScreenUtil().setHeight(100),
          width: ScreenUtil().setWidth(100),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Colors.white60, Colors.white70])),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              // borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: NetworkImage(dumyUser.picturePath),
                  fit: BoxFit.cover,
                  scale: 1),
            ),
          ),
        )
      ],
    );
  }
}
