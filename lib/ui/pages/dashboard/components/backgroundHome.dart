part of './../../pages.dart';

class BackgroundHome extends StatelessWidget {
  const BackgroundHome({
    Key key,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(ThemeApplication.imageAsset+"home_top.png"),
          ),
          child
        ],
      ),
    );
  }
}
