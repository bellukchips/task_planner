part of './../../pages.dart';

class BackgroundAddTask extends StatelessWidget {
  const BackgroundAddTask({
    Key key,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        children: [
          Positioned(
              left: 0,
              top: 50,
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, '/dashboard');
                },
                child: Image.asset(
                  ThemeApplication.imageAsset + "icon_back.png",
                ),
              )),
          child
        ],
      ),
    );
  }
}
