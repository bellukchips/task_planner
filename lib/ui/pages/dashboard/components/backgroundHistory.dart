part of './../../pages.dart';

class BackgroundHistory extends StatelessWidget {
  const BackgroundHistory({
    Key key,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        height: size.height,
        width: double.infinity,
        child:Stack(
          children: [
            Positioned(
                top: 0,
                left: 0,
                child:Image.asset(ThemeApplication.imageAsset+"history_top.png")
            ),

            child
          ],
        )
    );
  }
}