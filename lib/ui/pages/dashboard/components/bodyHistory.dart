part of './../../pages.dart';

class BodyHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: [HeaderHistory(), CardHistory()],
      ),
    );
  }
}

class CardHistory extends StatelessWidget {
  const CardHistory({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 2 * defaultMargin - 100,
      margin: EdgeInsets.only(top: 50),
      child: ListView(
          physics: BouncingScrollPhysics(),
          children: dumyTask
              .map((e) => Padding(
                  padding:
                      EdgeInsets.only(bottom: (e == dumyTask.last) ? 100 : 0),
                  child: (e.status == false)
                      ? CardTask(
                          color: ThemeApplication.purpleAccent,
                          title: e.title,
                          lastest: "",
                          time: e.time.dateAndTime,
                          fontColor: ThemeApplication.purpleDark,
                          fontColorTitle: ThemeApplication.purple,
                          fontSizeTitle: ScreenUtil().setSp(30),
                          fontSizeSub: ScreenUtil().setSp(23),
                        )
                      : Container()))
              .toList()),
    );
  }
}

class HeaderHistory extends StatelessWidget {
  const HeaderHistory({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50, left: 130),
      child: Text(
        "History your\ntask",
        style: ThemeApplication.greyPoppins.copyWith(
            fontSize: ScreenUtil().setSp(50, allowFontScalingSelf: true),
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
