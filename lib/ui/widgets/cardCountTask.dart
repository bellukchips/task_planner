part of 'widgets.dart';


class CardCountTask extends StatelessWidget {
  final double height;
  final double width;
  final double fontSizeTitle;
  final String title;
  final String subTitle;
  final double fontSizeNumber;
  final Color color;
  CardCountTask({this.height, this.width, this.fontSizeTitle, this.fontSizeNumber,this.title,this.subTitle,this.color});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: color,
      ),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 110,
              child: Text(
                title,
                style: ThemeApplication.whitePoppins.copyWith(
                    fontSize:
                   fontSizeTitle,fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
            Container(
              width: 110,
              margin: EdgeInsets.only(top: 2),
              child: Text(
               subTitle,
                style: ThemeApplication.whitePoppins.copyWith(
                    fontSize:fontSizeNumber),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
