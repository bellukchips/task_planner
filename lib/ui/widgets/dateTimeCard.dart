part of 'widgets.dart';

class DateTimeCard extends StatelessWidget {
  final bool isSelected;
  final double width;
  final double height;
  final DateTime date;
  final Function onTap;
  DateTimeCard(this.date,
      {this.isSelected = false, this.width, this.height, this.onTap});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(
                color: isSelected ? Colors.transparent : Color(0xffe4e4e4)),
            color: isSelected
                ? ThemeApplication.bluegreyAccent2
                : Colors.transparent),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(date.shortDayName + "\n" + date.monthName,
                textAlign: TextAlign.center,
                style: ThemeApplication.whitePoppins.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: ThemeApplication.greenDark)),
            SizedBox(
              height: 6,
            ),
            Text(
              date.day.toString(),
              style: ThemeApplication.whitePoppins.copyWith(
                  color: ThemeApplication.greenDark,
                  fontSize: 16,
                  fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }
}
