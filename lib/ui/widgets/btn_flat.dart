part of 'widgets.dart';

class ButtonFlat extends StatelessWidget {
  final String title;
  final Color color;
  final Function onPressed;
  final double width;
  final double height;
  final EdgeInsetsGeometry margin;
  final TextStyle textStyle;
  final Widget image;
  ButtonFlat(
      {this.title,
      this.color,
      this.onPressed,
      this.height,
      this.width,
      this.margin,
      this.textStyle,
      this.image});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin,
      child: RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                  width: image == null
                      ? getProportionateScreenWidth(0)
                      : getProportionateScreenWidth(38),
                  child: image),
              SizedBox(
                width: getProportionateScreenWidth(155),
                child: Text(
                  title,
                  overflow: TextOverflow.visible,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: textStyle,
                ),
              ),
            ],
          ),
          color: color,
          onPressed: onPressed),
    );
  }
}
