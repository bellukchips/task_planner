part of 'widgets.dart';

class CardTask extends StatelessWidget {
  CardTask(
      {Key key,
      this.color,
      this.title,
      this.time,
      this.lastest,
      this.decoration,
      this.fontColor,
      this.fontColorTitle,
      this.fontSizeTitle,
      this.onTap,
      this.fontSizeSub})
      : super(key: key);
  final String title;
  final String time;
  final String lastest;
  final Color color;
  final Color fontColor;
  final Color fontColorTitle;
  final TextDecoration decoration;
  final double fontSizeTitle;
  final double fontSizeSub;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        height: 100,
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: color),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 180,
                    child: Text(
                      title,
                      style: ThemeApplication.robotoFonts.copyWith(
                          color: fontColorTitle,
                          fontSize: fontSizeTitle,
                          fontWeight: FontWeight.bold,
                          decoration: decoration),
                      maxLines: 1,
                      overflow: TextOverflow.visible,
                    ),
                  ),
                  Text(
                    lastest,
                    style: ThemeApplication.robotoFonts
                        .copyWith(color: fontColor, fontSize: fontSizeSub),
                  ),
                ],
              ),
              Text(
                time,
                style: ThemeApplication.robotoFonts
                    .copyWith(color: fontColor, fontSize: fontSizeSub),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                "Detail",
                style: ThemeApplication.robotoFonts
                    .copyWith(color: fontColorTitle, fontSize: fontSizeSub),
                textAlign: TextAlign.end,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
