part of 'models.dart';

class TaskModel extends Equatable {
  final String title;
  final String description;
  final DateTime time;
  final int id;
  final Color cardColor;
  final UserModel user;
  final bool status;

  TaskModel(
      {this.title,
      this.description,
      this.time,
      this.id,
      this.cardColor,
      this.user,
      this.status});
  TaskModel copyWith({
    String title,
    String description,
    DateTime time,
    Color cardColor,
    bool status
  })=> TaskModel(
    title: title ?? this.title,
    description: description ?? this.description,
    cardColor:  cardColor ?? this.cardColor,
    status: status ?? this.status,
    time: time ?? this.time
  );
  @override
  // TODO: implement props
  List<Object> get props =>
      [title, description, time, id, cardColor, user, status];
}

List<TaskModel> dumyTask = [
  TaskModel(
    id: 1,
    user:UserModel(name: dumyUser.name),
    title: 'Project Client',
    status: true,
    description: 'project client crewet banget deh',
    time: DateTime.now(),
    cardColor: ThemeApplication.bluegreyAccent2,
  ),
  TaskModel(
    id: 2,
    user:UserModel(name: dumyUser.name),
    title: 'Project Client 2',
    status: true,
    description: 'project client crewet banget deh',
    time: DateTime.now(),
    cardColor: ThemeApplication.bluegreyAccent2,
  ),
  TaskModel(
    id: 3,
    user:UserModel(name: dumyUser.name),
    title: 'Project Client 3',
    status: false,
    description: 'project client crewet banget deh',
    time: DateTime.now(),
    cardColor: ThemeApplication.bluegreyAccent2,
  ),
  TaskModel(
    id: 4,
    user:UserModel(name: dumyUser.name),
    title: 'Project Client 4',
    status: false,
    description: 'project client crewet banget deh',
    time: DateTime.now(),
    cardColor: ThemeApplication.bluegreyAccent2,
  ),
];
