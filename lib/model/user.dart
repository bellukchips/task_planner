part of 'models.dart';

class UserModel extends Equatable {
  final String name;
  final String email;
  final String id;
  final String picturePath;

  UserModel({this.name, this.email, this.id, this.picturePath});
  @override
  // TODO: implement props
  List<Object> get props =>[name, email, id, picturePath];
}

UserModel dumyUser = UserModel(
  email: 'lukmankubang@gmail.com',
  name:'lukman kubang',
  id: 'acs',
  picturePath: 'http://4.bp.blogspot.com/-sadwqWIC-zg/VJYfKCL5YZI/AAAAAAAALO8/XqyOEnIgUjM/s1600/1619496_10152705202199771_7845143923449467057_n.jpg'

);