import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:task_planner/shared/shared.dart';

part 'task.dart';
part 'user.dart';
