part of 'router.dart';

class RouterGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    const String getStarted = '/';
    const String signIn = '/signIn';
    const String dashboard = '/dashboard';
    const String addNewTask = '/addNewTask';
    const String detailTask = '/detailTask';
    switch (settings.name) {
      case getStarted:
        return PageTransition(
            child: GetStarted(), type: PageTransitionType.leftToRight);
        break;
      case signIn:
        return PageTransition(
            child: SignInPage(), type: PageTransitionType.rightToLeft);
      case dashboard:
        return MaterialPageRoute(builder: (_) => DashboardPage());
      case addNewTask:
        return PageTransition(
            child: AddTaskPage(), type: PageTransitionType.bottomToTop);
      case detailTask:
        return PageTransition(
            child: DetailTask(), type: PageTransitionType.rightToLeft);
      default:
        return MaterialPageRoute(builder: (_) => DashboardPage());
    }
  }
}
