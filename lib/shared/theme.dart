part of 'shared.dart';

double defaultMargin = 28;

class ThemeApplication {
  ///
  ///Color palet
  static Color blueGrey = Color(0xff9EC5D1);
  static Color blueGreydark = Color(0xff97BCC0);
  static Color redAccent = Color(0xffF0A7A7);
  static Color red = Color(0xffFA8383); //  select color card
  static Color orangeAccent1 = Color(0xffFF8A77); // use on card task
  static Color orangeAccent2 =
      Color(0xffFFCAC2); // use on card task  & select color card
  static Color orangeAccent3 = Color(0xffFF9988); // use on card task
  static Color pink = Color(0xffFD9BBF);
  static Color pinkAccent = Color(0xffFF82AF);
  static Color blueGreyAccent = Color(0xffBBD5DE);
  static Color blueGreyAccent1 = Color(0xffBDE0EB);
  static Color bluegreyAccent2 =
      Color(0xffD5EEEE); // use on card task & select color card
  static Color bluegreyDark = Color(0xff90A5A5); // use on card task
  static Color greenDark =
      Color(0xff479696); // use on card task & select color card
  static Color green = Color(0xff37A88D);
  static Color greenAccent = Color(0xff7AC4B3);
  static Color purpleAccent =
      Color(0xffD8CDFF); // use on card task  & select color card
  static Color purpleDark =
      Color(0xff988EBC); // use on card task  & select color card
  static Color purple = Color(0xff957BF4); // use on card task
  static Color black = Colors.black;
  static Color fontColorPrimary = Color(0xff676666);
  static Color grey = Color(0xffF2F2F2);

  ///
  ///
  ///asset image
  static String imageAsset = "assets/img/";

  ///
  ///
  ///
  static TextStyle greyPoppins =
      GoogleFonts.poppins().copyWith(color: fontColorPrimary);
  static TextStyle whitePoppins =
      GoogleFonts.poppins().copyWith(color: Colors.white);
  static TextStyle blackPoppins = GoogleFonts.poppins().copyWith(color: black);
  static TextStyle robotoFonts = GoogleFonts.roboto();
}
