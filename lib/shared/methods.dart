part of 'shared.dart';

///
///alert dialog
Future<Widget> alertLoading(BuildContext context) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0)), //this right here
          child: Container(
            height: 130,
            width: 100,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20, bottom: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Please wait",
                    style: ThemeApplication.blackPoppins
                        .copyWith(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              ThemeApplication.blueGrey),
                        ),
                      ),
                      SizedBox(width: 20),
                      TyperAnimatedTextKit(
                        text: ['Loading...'],
                        textAlign: TextAlign.center,
                        textStyle: ThemeApplication.blackPoppins
                            .copyWith(fontSize: 15),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      });
}

///
///
///
