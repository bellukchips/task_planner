import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:task_planner/shared/shared.dart';
import 'router/router.dart';
import 'ui/pages/pages.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Task Planner: To do List',
      home: GetStarted(),
      onGenerateRoute: RouterGenerator.generateRoute,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: ThemeApplication.grey,
          scaffoldBackgroundColor: Colors.white),
    );
  }
}
